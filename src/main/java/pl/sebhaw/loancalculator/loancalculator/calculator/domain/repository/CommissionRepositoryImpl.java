package pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository;

import org.springframework.stereotype.Repository;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.Commission;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CommissionRepositoryImpl implements CommissionRepository {

    @Override
    public List<Commission> getCommissions() {
        return getCommissionsList();
    }

    private List<Commission> getCommissionsList() {
        List<Commission> commissionList = new ArrayList<>();

        commissionList.add(Commission.builder().vehicleAge(0).minLoanLength(6).maxLoanLength(12).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(0).minLoanLength(13).maxLoanLength(24).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(0).minLoanLength(25).maxLoanLength(36).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(0).minLoanLength(37).maxLoanLength(48).commissionValue(BigDecimal.valueOf(3)).build());
        commissionList.add(Commission.builder().vehicleAge(0).minLoanLength(49).maxLoanLength(60).commissionValue(BigDecimal.valueOf(3)).build());

        commissionList.add(Commission.builder().vehicleAge(1).minLoanLength(6).maxLoanLength(12).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(1).minLoanLength(13).maxLoanLength(24).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(1).minLoanLength(25).maxLoanLength(36).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(1).minLoanLength(37).maxLoanLength(48).commissionValue(BigDecimal.valueOf(3)).build());
        commissionList.add(Commission.builder().vehicleAge(1).minLoanLength(49).maxLoanLength(60).commissionValue(BigDecimal.valueOf(3)).build());

        commissionList.add(Commission.builder().vehicleAge(2).minLoanLength(6).maxLoanLength(12).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(2).minLoanLength(13).maxLoanLength(24).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(2).minLoanLength(25).maxLoanLength(36).commissionValue(BigDecimal.valueOf(2.5)).build());
        commissionList.add(Commission.builder().vehicleAge(2).minLoanLength(37).maxLoanLength(48).commissionValue(BigDecimal.valueOf(3)).build());
        commissionList.add(Commission.builder().vehicleAge(2).minLoanLength(49).maxLoanLength(60).commissionValue(BigDecimal.valueOf(3)).build());

        commissionList.add(Commission.builder().vehicleAge(3).minLoanLength(6).maxLoanLength(12).commissionValue(BigDecimal.valueOf(3)).build());
        commissionList.add(Commission.builder().vehicleAge(3).minLoanLength(13).maxLoanLength(24).commissionValue(BigDecimal.valueOf(3)).build());
        commissionList.add(Commission.builder().vehicleAge(3).minLoanLength(25).maxLoanLength(36).commissionValue(BigDecimal.valueOf(3)).build());
        commissionList.add(Commission.builder().vehicleAge(3).minLoanLength(37).maxLoanLength(48).commissionValue(BigDecimal.valueOf(4)).build());
        commissionList.add(Commission.builder().vehicleAge(3).minLoanLength(49).maxLoanLength(60).commissionValue(BigDecimal.valueOf(4)).build());

        commissionList.add(Commission.builder().vehicleAge(4).minLoanLength(6).maxLoanLength(12).commissionValue(BigDecimal.valueOf(4)).build());
        commissionList.add(Commission.builder().vehicleAge(4).minLoanLength(13).maxLoanLength(24).commissionValue(BigDecimal.valueOf(4)).build());
        commissionList.add(Commission.builder().vehicleAge(4).minLoanLength(25).maxLoanLength(36).commissionValue(BigDecimal.valueOf(4)).build());
        commissionList.add(Commission.builder().vehicleAge(4).minLoanLength(37).maxLoanLength(48).commissionValue(BigDecimal.valueOf(4.5)).build());
        commissionList.add(Commission.builder().vehicleAge(4).minLoanLength(49).maxLoanLength(60).commissionValue(BigDecimal.valueOf(5)).build());

        commissionList.add(Commission.builder().vehicleAge(5).minLoanLength(6).maxLoanLength(12).commissionValue(BigDecimal.valueOf(5)).build());
        commissionList.add(Commission.builder().vehicleAge(5).minLoanLength(13).maxLoanLength(24).commissionValue(BigDecimal.valueOf(5)).build());
        commissionList.add(Commission.builder().vehicleAge(5).minLoanLength(25).maxLoanLength(36).commissionValue(BigDecimal.valueOf(5)).build());
        commissionList.add(Commission.builder().vehicleAge(5).minLoanLength(37).maxLoanLength(48).commissionValue(BigDecimal.valueOf(6)).build());
        commissionList.add(Commission.builder().vehicleAge(5).minLoanLength(49).maxLoanLength(60).commissionValue(BigDecimal.valueOf(6)).build());

        return commissionList;
    }
}
