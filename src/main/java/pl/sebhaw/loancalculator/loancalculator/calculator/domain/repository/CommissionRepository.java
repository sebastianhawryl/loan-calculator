package pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository;

import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.Commission;

import java.util.List;

public interface CommissionRepository {

    List<Commission> getCommissions();
}
