package pl.sebhaw.loancalculator.loancalculator.calculator.web;

public class LoanValidatorPrinter {

    public static final String LOAN_LENGTH_BETWEEN_6_AND_60_MONTHS_MESSAGE = "Length of loan may be between 6 and 60 months";
    public static final String LOAN_ENTRY_FEE_POSITIVE_NUMBER = "Loan entry fee should be positive number";
    public static final String LOAN_FINANCING_AMOUNT_POSITIVE_NUMBER = "Loan financing amount should be positive number";

    public static String printLoanLengthBetween6And60Months() {
        return LOAN_LENGTH_BETWEEN_6_AND_60_MONTHS_MESSAGE;
    }

    public static String printEntryFeePositiveNumber() {
        return LOAN_ENTRY_FEE_POSITIVE_NUMBER;
    }

    public static String printFinancingAmountPositiveNumber() {
        return LOAN_FINANCING_AMOUNT_POSITIVE_NUMBER;
    }
}
