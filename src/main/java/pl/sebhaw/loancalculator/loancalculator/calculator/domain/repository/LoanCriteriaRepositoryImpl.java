package pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository;

import org.springframework.stereotype.Component;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.Commission;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanCriteria;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.Margin;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Component
public class LoanCriteriaRepositoryImpl implements LoanCriteriaRepository {

    private Calendar calendar;
    private final LoanCriteriaSource loanCriteriaSource;

    public LoanCriteriaRepositoryImpl(LoanCriteriaSource loanCriteriaSource) {
        this.calendar = new GregorianCalendar();
        this.loanCriteriaSource = loanCriteriaSource;
    }

    @Override
    public LoanCriteria getLoanCriterias() {
        return loanCriteriaSource.getAllLoanCriteria();
    }

    @Override
    public BigDecimal getLoanMargin(Integer productionYear, Integer loanLength) {
        Margin margin = getLoanCriterias().getLoanMargin().stream()
                .filter(c -> c.getMinLoanLength() <= loanLength &&
                        c.getMaxLoanLength() >= loanLength &&
                        c.getVehicleAge() == getVehicleAgeFromProductionYear(productionYear))
                .findAny().get();

        return margin.getMarginValue();
    }

    @Override
    public BigDecimal getLoanCommission(Integer productionYear, Integer loanLength) {
        Commission commission = getLoanCriterias().getLoanCommission().stream()
                .filter(c -> c.getMinLoanLength() <= loanLength &&
                        c.getMaxLoanLength() >= loanLength &&
                        c.getVehicleAge() == getVehicleAgeFromProductionYear(productionYear))
                .findAny().get();

        return commission.getCommissionValue();
    }

    private Integer getVehicleAgeFromProductionYear(Integer productionYear) {
        return Math.round(calendar.get(Calendar.YEAR) - productionYear);
    }
}
