package pl.sebhaw.loancalculator.loancalculator.calculator.domain.model;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

@Value
@Builder
public class LoanCriteria {

    private List<Margin> loanMargin;
    private List<Commission> loanCommission;
}
