package pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository;

import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanCriteria;

import java.math.BigDecimal;

public interface LoanCriteriaRepository {

    LoanCriteria getLoanCriterias();

    BigDecimal getLoanMargin(Integer productionYear, Integer loanLength);

    BigDecimal getLoanCommission(Integer productionYear, Integer loanLength);
}
