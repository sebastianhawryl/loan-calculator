package pl.sebhaw.loancalculator.loancalculator.calculator.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Commission {

    private Integer vehicleAge;
    private Integer minLoanLength;
    private Integer maxLoanLength;
    private BigDecimal commissionValue;
}
