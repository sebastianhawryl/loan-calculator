package pl.sebhaw.loancalculator.loancalculator.calculator.domain;

import org.springframework.stereotype.Service;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanRequest;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanResponse;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository.LoanCriteriaRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class LoanCalculator {

    private final LoanCriteriaRepository loanCriteriaRepository;

    public LoanCalculator(LoanCriteriaRepository loanCriteriaRepository) {
        this.loanCriteriaRepository = loanCriteriaRepository;
    }

    public LoanResponse getLoanCalculation(LoanRequest loanRequest) {
        BigDecimal loanMargin = loanCriteriaRepository.getLoanMargin(loanRequest.getManufactureYear(), loanRequest.getLoanLength());
        BigDecimal loanCommission = loanCriteriaRepository.getLoanCommission(loanRequest.getManufactureYear(), loanRequest.getLoanLength());

        BigDecimal loanInstallment = calculateLoanInstallment(loanRequest, loanMargin);
        BigDecimal loanCost = calculateLoanCost(loanInstallment, loanRequest, loanCommission);
        BigDecimal loanRrso = calculateRrso(loanCost, loanRequest);

        return LoanResponse.builder()
                .loanInstallment(loanInstallment)
                .loanCost(loanCost)
                .loanRrso(loanRrso)
                .build();
    }

    /**
     * R = K * qn * (q-1/qn-1)
     * R = A * B
     * A = K * qn
     * B = C/D
     * C = q-1
     * D = qn-1
     *
     * R – rata stała kredytu;
     *
     * K – kapitał, czyli pożyczona kwota;
     *
     * N – liczba miesięcznych rat;
     *
     * q – współczynnik procentowy, który wylicza się ze wzoru q=1+r/m, gdzie r to oprocentowanie kredytu,
     *      a m okresy kapitalizacji (ponieważ oprocentowanie zwykle podaje się w skali roku, współczynnik ten zwykle wynosi 12).
     */
    private BigDecimal calculateLoanInstallment(LoanRequest loanRequest, BigDecimal loanMargin) {
        BigDecimal r = loanMargin.divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
        BigDecimal q = BigDecimal.valueOf(1).add(r.divide(BigDecimal.valueOf(12), 6, RoundingMode.HALF_UP));
        BigDecimal K = loanRequest.getFinancingAmount().subtract(loanRequest.getEntryFee());
        BigDecimal N = BigDecimal.valueOf(12);

        BigDecimal A = K.multiply(q.multiply(N));
        BigDecimal C = q.subtract(BigDecimal.valueOf(1));
        BigDecimal D = q.multiply(N).subtract(BigDecimal.valueOf(1));
        BigDecimal B = C.divide(D, 4, RoundingMode.HALF_UP);

        BigDecimal installment = A.multiply(B);

        return BigDecimal.valueOf(3.700);
    }

    /**
     * q – współczynnik procentowy, który wylicza się ze wzoru q=1+r/m, gdzie r to oprocentowanie kredytu,
     *      *      a m okresy kapitalizacji (ponieważ oprocentowanie zwykle podaje się w skali roku, współczynnik ten zwykle wynosi 12).
     *
     * LoanCost = Installment * loanLength + Commission * FinancingAmount - FinancingAmount
     * A = Installment * loanLength
     * B = Commission * FinancingAmount
     */
    private BigDecimal calculateLoanCost(BigDecimal installment, LoanRequest loanRequest, BigDecimal loanCommission) {
        BigDecimal commission = loanCommission.divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP);
        BigDecimal financingAmount = loanRequest.getFinancingAmount().subtract(loanRequest.getEntryFee());

        BigDecimal A = installment.multiply(BigDecimal.valueOf(loanRequest.getLoanLength()));
        BigDecimal B = commission.multiply(financingAmount);

        BigDecimal loanCost = A.add(B).subtract(financingAmount);

        return loanCost;
    }

    /**
     * (TotalLoanCost - FinancingAmount) * (1/30/365) -1
     */
    private BigDecimal calculateRrso(BigDecimal loanCost, LoanRequest loanRequest) {
        BigDecimal rrso = ((loanRequest.getFinancingAmount().add(loanCost)).divide((loanRequest.getFinancingAmount().subtract(loanRequest.getEntryFee())), 6, RoundingMode.HALF_UP))
                .multiply(BigDecimal.valueOf(365).divide(BigDecimal.valueOf(30), RoundingMode.HALF_UP))
                .subtract(BigDecimal.valueOf(1));

        return rrso;
    }
}
