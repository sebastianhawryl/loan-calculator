package pl.sebhaw.loancalculator.loancalculator.calculator.web;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Getter
public class LoanResponseDTO {

    @NotNull
    private BigDecimal loanInstallment;

    @NotNull
    private BigDecimal loanRrso;

    @NotNull
    private BigDecimal loanCost;
}
