package pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository;

import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.Margin;

import java.util.List;

public interface MarginRepository {

    List<Margin> getMargins();
}
