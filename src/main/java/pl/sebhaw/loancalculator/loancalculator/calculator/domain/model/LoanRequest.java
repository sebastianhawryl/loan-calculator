package pl.sebhaw.loancalculator.loancalculator.calculator.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoanRequest {

    private BigDecimal financingAmount;
    private Integer loanLength;
    private BigDecimal entryFee;
    private Integer manufactureYear;
}
