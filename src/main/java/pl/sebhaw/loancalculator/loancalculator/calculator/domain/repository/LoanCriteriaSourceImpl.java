package pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository;

import org.springframework.stereotype.Repository;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanCriteria;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository.CommissionRepository;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository.LoanCriteriaSource;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository.MarginRepository;

@Repository
public class LoanCriteriaSourceImpl implements LoanCriteriaSource {

    private final MarginRepository marginRepository;
    private final CommissionRepository commissionRepository;

    public LoanCriteriaSourceImpl(MarginRepository marginRepository, CommissionRepository commissionRepository) {
        this.marginRepository = marginRepository;
        this.commissionRepository = commissionRepository;
    }

    @Override
    public LoanCriteria getAllLoanCriteria() {
        return LoanCriteria.builder()
                .loanMargin(marginRepository.getMargins())
                .loanCommission(commissionRepository.getCommissions())
                .build();
    }
}
