package pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository;

import org.springframework.stereotype.Repository;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.Margin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MarginRepositoryImpl implements MarginRepository{

    @Override
    public List<Margin> getMargins() {
        return getMarginsList();
    }

    private List<Margin> getMarginsList() {
        List<Margin> marginsList = new ArrayList<>();

        marginsList.add(Margin.builder().vehicleAge(0).minLoanLength(6).maxLoanLength(12).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(0).minLoanLength(13).maxLoanLength(24).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(0).minLoanLength(25).maxLoanLength(36).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(0).minLoanLength(37).maxLoanLength(48).marginValue(BigDecimal.valueOf(5.5)).build());
        marginsList.add(Margin.builder().vehicleAge(0).minLoanLength(49).maxLoanLength(60).marginValue(BigDecimal.valueOf(5.5)).build());

        marginsList.add(Margin.builder().vehicleAge(1).minLoanLength(6).maxLoanLength(12).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(1).minLoanLength(13).maxLoanLength(24).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(1).minLoanLength(25).maxLoanLength(36).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(1).minLoanLength(37).maxLoanLength(48).marginValue(BigDecimal.valueOf(5.5)).build());
        marginsList.add(Margin.builder().vehicleAge(1).minLoanLength(49).maxLoanLength(60).marginValue(BigDecimal.valueOf(5.5)).build());

        marginsList.add(Margin.builder().vehicleAge(2).minLoanLength(6).maxLoanLength(12).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(2).minLoanLength(13).maxLoanLength(24).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(2).minLoanLength(25).maxLoanLength(36).marginValue(BigDecimal.valueOf(5)).build());
        marginsList.add(Margin.builder().vehicleAge(2).minLoanLength(37).maxLoanLength(48).marginValue(BigDecimal.valueOf(5.5)).build());
        marginsList.add(Margin.builder().vehicleAge(2).minLoanLength(49).maxLoanLength(60).marginValue(BigDecimal.valueOf(5.5)).build());

        marginsList.add(Margin.builder().vehicleAge(3).minLoanLength(6).maxLoanLength(12).marginValue(BigDecimal.valueOf(6)).build());
        marginsList.add(Margin.builder().vehicleAge(3).minLoanLength(13).maxLoanLength(24).marginValue(BigDecimal.valueOf(6)).build());
        marginsList.add(Margin.builder().vehicleAge(3).minLoanLength(25).maxLoanLength(36).marginValue(BigDecimal.valueOf(6)).build());
        marginsList.add(Margin.builder().vehicleAge(3).minLoanLength(37).maxLoanLength(48).marginValue(BigDecimal.valueOf(6.5)).build());
        marginsList.add(Margin.builder().vehicleAge(3).minLoanLength(49).maxLoanLength(60).marginValue(BigDecimal.valueOf(6.5)).build());

        marginsList.add(Margin.builder().vehicleAge(4).minLoanLength(6).maxLoanLength(12).marginValue(BigDecimal.valueOf(7)).build());
        marginsList.add(Margin.builder().vehicleAge(4).minLoanLength(13).maxLoanLength(24).marginValue(BigDecimal.valueOf(7)).build());
        marginsList.add(Margin.builder().vehicleAge(4).minLoanLength(25).maxLoanLength(36).marginValue(BigDecimal.valueOf(7)).build());
        marginsList.add(Margin.builder().vehicleAge(4).minLoanLength(37).maxLoanLength(48).marginValue(BigDecimal.valueOf(8)).build());
        marginsList.add(Margin.builder().vehicleAge(4).minLoanLength(49).maxLoanLength(60).marginValue(BigDecimal.valueOf(8)).build());

        marginsList.add(Margin.builder().vehicleAge(5).minLoanLength(6).maxLoanLength(12).marginValue(BigDecimal.valueOf(8)).build());
        marginsList.add(Margin.builder().vehicleAge(5).minLoanLength(13).maxLoanLength(24).marginValue(BigDecimal.valueOf(8)).build());
        marginsList.add(Margin.builder().vehicleAge(5).minLoanLength(25).maxLoanLength(36).marginValue(BigDecimal.valueOf(8)).build());
        marginsList.add(Margin.builder().vehicleAge(5).minLoanLength(37).maxLoanLength(48).marginValue(BigDecimal.valueOf(8.5)).build());
        marginsList.add(Margin.builder().vehicleAge(5).minLoanLength(49).maxLoanLength(60).marginValue(BigDecimal.valueOf(8.5)).build());

        return marginsList;
    }
}
