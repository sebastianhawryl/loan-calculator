package pl.sebhaw.loancalculator.loancalculator.calculator.domain.repository;

import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanCriteria;

public interface LoanCriteriaSource {

    LoanCriteria getAllLoanCriteria();
}
