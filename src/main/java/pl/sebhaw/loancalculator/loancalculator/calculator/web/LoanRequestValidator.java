package pl.sebhaw.loancalculator.loancalculator.calculator.web;

import java.math.BigDecimal;

public class LoanRequestValidator {

    private LoanValidatorPrinter loanValidatorPrinter;

    public LoanRequestValidator() {
        this.loanValidatorPrinter = new LoanValidatorPrinter();
    }

    public Integer validateLoanLength(Integer loanLength) {
        if(loanLength < 6 || loanLength > 60) {
            throw new IllegalArgumentException(loanValidatorPrinter.printLoanLengthBetween6And60Months());
        } else return loanLength;
    }

    public BigDecimal validateEntryFee(BigDecimal entryFee) {
        if(entryFee.compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new IllegalArgumentException(loanValidatorPrinter.printEntryFeePositiveNumber());
        } else return entryFee;
    }

    public BigDecimal validateFinancingAmount(BigDecimal financingAmount) {
        if(financingAmount.compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new IllegalArgumentException(loanValidatorPrinter.printFinancingAmountPositiveNumber());
        } else return financingAmount;
    }
}
