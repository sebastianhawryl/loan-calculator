package pl.sebhaw.loancalculator.loancalculator.calculator.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.LoanCalculator;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanRequest;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanResponse;

import javax.validation.Valid;

@RestController
@RequestMapping("/loan")
@CrossOrigin
public class LoanController {

    private LoanRequestMapper loanRequestMapper;
    private final LoanCalculator loanCalculator;

    public LoanController(LoanCalculator loanCalculator) {
        this.loanCalculator = loanCalculator;
        this.loanRequestMapper = new LoanRequestMapper();
    }

    @PostMapping("/calculate")
    public ResponseEntity calculate(@Valid @RequestBody LoanRequestDTO loanRequestDTO) {
        LoanRequest loanRequest = loanRequestMapper.dtoToEntity(loanRequestDTO);

        LoanResponse loanResponse = loanCalculator.getLoanCalculation(loanRequest);

        return ResponseEntity.ok().body(loanRequestMapper.entityToDto(loanResponse));
    }
}
