package pl.sebhaw.loancalculator.loancalculator.calculator.web;

import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanRequest;
import pl.sebhaw.loancalculator.loancalculator.calculator.domain.model.LoanResponse;

public class LoanRequestMapper {

    private final LoanRequestValidator requestValidator;

    public LoanRequestMapper() {
        this.requestValidator = new LoanRequestValidator();
    }

    public LoanRequest dtoToEntity(LoanRequestDTO loanRequestDTO) {
        LoanRequest loanRequest = LoanRequest.builder()
                .financingAmount(requestValidator.validateFinancingAmount(loanRequestDTO.getFinancingAmount()))
                .loanLength(requestValidator.validateLoanLength(loanRequestDTO.getLoanLength()))
                .entryFee(requestValidator.validateEntryFee(loanRequestDTO.getEntryFee()))
                .manufactureYear(loanRequestDTO.getManufactureYear())
                .build();

        return loanRequest;
    }

    public LoanResponseDTO entityToDto(LoanResponse loanResponse) {
        LoanResponseDTO loanResponseDTO = LoanResponseDTO.builder()
                .loanRrso(loanResponse.getLoanRrso())
                .loanCost(loanResponse.getLoanCost())
                .loanInstallment(loanResponse.getLoanInstallment())
                .build();

        return loanResponseDTO;
    }
}
