package pl.sebhaw.loancalculator.loancalculator.calculator.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanResponse {

    private BigDecimal loanInstallment;
    private BigDecimal loanRrso;
    private BigDecimal loanCost;
}
