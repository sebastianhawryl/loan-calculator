package pl.sebhaw.loancalculator.loancalculator.calculator.web;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Getter
public class LoanRequestDTO {

    @NotNull
    private BigDecimal financingAmount;

    @NotNull
    private Integer loanLength;

    @NotNull
    private BigDecimal entryFee;

    @NotNull
    private Integer manufactureYear;
}
